# FaceRecognition
CV2 Face Recognition

Pasos:

1. Crear una carpeta llamada 'att_faces', dentro de esta crear la carpeta llamada 'orl_faces', luego dentro de esta crear una carpeta con su nombre. Por ejemplo, la carpeta PatricioEstrella y correr el siguiente comando, tomará 100 imagenes y las guardara en la carpeta PatricioEstrella

```
python capture.py PatricioEstrella
```

2. Ya teniendo 100 imagenes el programa ya reconocera tu rostro, correr el siguiente comando

```
python reconocimiento.py
```
